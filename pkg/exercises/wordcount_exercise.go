package exercises

import (
	"strings"

	"golang.org/x/tour/wc"
)

func WordCount(s string) map[string]int {
	words := strings.Fields(s)
	count := map[string]int{}
	for _, word := range words {
		if _, ok := count[word]; ok {
			count[word] += 1
		} else {
			count[word] = 1
		}
	}
	return count
}

func WCTest() bool {
	wc.Test(WordCount)
	return true
}
