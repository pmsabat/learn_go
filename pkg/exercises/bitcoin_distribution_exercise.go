package exercises

func coinsForUser(user string, max int) int {
	total := 0
	for index := range user {
		switch string(user[index]) {
		case "a", "A":
			total += 1
		case "e", "E":
			total += 1
		case "i", "I":
			total += 2
		case "o", "O":
			total += 3
		case "u", "U":
			total += 4
		}
	}
	if total > max {
		total = max
	}
	return total
}

func DistributeBitcoin(users []string, bitcoin int) map[string]int {
	distribution := make(map[string]int, len(users))
	for _, user := range users {
		share := coinsForUser(user, 10)
		// println(fmt.Sprintf("The share for the user %s was: %d", user, share))
		if share > bitcoin {
			share = bitcoin
			// println(fmt.Sprintf("The post check share for the user %s was: %d out of bitcoin %d", user, share, bitcoin))
		}
		distribution[user] = share
		bitcoin -= share

	}
	return distribution
}
