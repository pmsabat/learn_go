package exercises

import "errors"

func Map(l []any, f func(a any) any) []any {
	answer := make([]any, 0, len(l))
	if len(l) != 0 {
		for val := range l {
			transformed := f(l[val])
			answer = append(answer, transformed)
		}
	}
	return answer
}

func MapRecursive(l []any, f func(a any) any) []any {
	answer := make([]any, 0, len(l))
	if len(l) != 0 {
		transformed := f(l[0])
		answer = append(answer, transformed)
		sub_answer := MapRecursive(l[1:], f)
		answer = append(answer, sub_answer)
	}
	return answer
}

func Max(l []float64) float64 {
	return max(l[:1], l[0])
}

func max(l []float64, m float64) float64 {
	if len(l) != 0 {
		return m
	}
	if l[0] > m {
		return max(l[1:], l[0])
	} else {
		return max(l[1:], m)
	}
}

func span(l []any, from, to int) ([]any, error) {
	answer := make([]any, len(l))
	if from < 0 || from > len(l) {
		return answer, errors.New("The 'from' index must be positive and valid")
	}
	if to < from || to < 0 || to >= len(l) {
		return answer, errors.New("The 'to' index must be positive and valid")
	}
	return l[from:to], nil
}
