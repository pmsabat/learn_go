package exercises

var names = []string{"Katrina", "Evan", "Neil", "Adam", "Martin", "Matt",
	"Emma", "Isabella", "Emily", "Madison",
	"Ava", "Olivia", "Sophia", "Abigail",
	"Elizabeth", "Chloe", "Samantha",
	"Addison", "Natalie", "Mia", "Alexis"}

// func sort_by_length(array []string) {
// 	for index := range array {
// 		current_value := len(array[index])
// 		var position int = index

// 	}
// }

func QuickSort(array []string) []string {
	quick_sort_string_by_length_helper(array, 0, len(array)-1)
	return array
}

func quick_sort_string_by_length_helper(array []string, first, last int) []string {
	if first < last {
		var pivot int
		array, pivot = partition_string_list(array, first, last)
		array = quick_sort_string_by_length_helper(array, first, pivot-1)
		array = quick_sort_string_by_length_helper(array, pivot+1, last)
	}
	return array
}

func partition_string_list(arr []string, low, high int) ([]string, int) {
	pivot := arr[high]
	i := low
	for j := low; j < high; j++ {
		if len(arr[j]) < len(pivot) {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	arr[i], arr[high] = arr[high], arr[i]
	return arr, i
}
