package chapter6

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net"
	"time"
)

type Server struct {
	Payload []byte
	Retries uint8
	Timeout time.Duration
}

func (s Server) ListenAndServe(addr string) error {
	conn, err := net.ListenPacket("udp", addr)
	if err != nil {
		return err
	}
	defer func() { _ = conn.Close() }()

	log.Printf("Listening on %s... \n", conn.LocalAddr())
	return s.Serve(conn)
}

func (s Server) Serve(conn net.PacketConn) error {
	if conn == nil {
		return fmt.Errorf("The connection was nil!  Unable to serve!")
	}

	if s.Payload == nil {
		return errors.New("Payload is required")
	}

	if s.Retries == 0 {
		s.Retries = 10
	}

	if s.Timeout == 0 {
		s.Timeout = 6 * time.Second
	}

	var rrq TFTReadPacket

	for {
		buf := make([]byte, DatagramSize)
		_, addr, err := conn.ReadFrom(buf)
		if err != nil {
			return fmt.Errorf("Error reading from connection - %w", err)
		}

		err = rrq.UnMarshalBinary(buf)
		if err != nil {
			log.Printf("Invalid read request received from client, error - %w \n", err)
			continue
		}
		go s.Handle(addr.String(), rrq)
	}
}

func (s Server) Handle(clientAddr string, rrq TFTReadPacket) {
	log.Printf("Client[%s] requested file: %s \n", clientAddr, rrq.FileName)
	conn, err := net.Dial("udp", clientAddr)
	if err != nil {
		log.Printf("The connection to the client failed - %s \n", err)
		return
	}

	defer func() { _ = conn.Close() }()

	var (
		ackPkt  TFTPAckPacket
		errPkt  TFTPErrorPacket
		dataPkt = TFTPDataPacket{OpCode: 3, Block: 1, Data: bytes.NewReader(s.Payload)}
		buf     = make([]byte, DatagramSize)
	)

	// For the next packet
	for n := DatagramSize; n == DatagramSize; {
		data, err := dataPkt.MarshalBinary()
		if err != nil {
			log.Printf("The datapacket could not be marshalled, err - %s \n", err)
			return
		}

		for i := s.Retries; i > 0; i-- {
			n, err = conn.Write(data)
			if err != nil {
				log.Printf("The datapacket errored on write to the connection - %s \n", err)
			}

			_ = conn.SetReadDeadline(time.Now().Add(time.Second * s.Timeout))

			_, err = conn.Read(buf)

			if err != nil {
				if nErr, ok := err.(net.Error); ok && nErr.Timeout() {
					continue // retry this
				}

				log.Printf("[%s] waiting for ack on data %v \n", clientAddr, err)
				return
			}

			switch {
			case ackPkt.UnMarshalBinary(buf) == nil:
				if uint16(ackPkt.blockNumber) == dataPkt.Block {
					continue
				}
			case errPkt.UnMarshalBinary(buf) == nil:
				log.Printf("[%s] Recieved error for %v \n", clientAddr, errPkt.errorMessage)
				return
			default:
				log.Printf("[%s] Bad Packet \n", clientAddr)
			}
		}

		log.Printf("[%s] exhausted retries \n", clientAddr)
		return
	}

	log.Printf("[%s] sent %d blocks", clientAddr, dataPkt.Block)
}
