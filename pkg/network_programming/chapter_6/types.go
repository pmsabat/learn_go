package chapter6

// import "bytes"

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"strings"
)

type Opcode uint16

const (
	DatagramSize = 516
	BlockSize    = DatagramSize - 4
)

const (
	OpRRQ Opcode = iota + 1
	OpWrq
	OpData
	OpAck
	OpErr
)

type ErrCode uint16

const (
	ErrUnknown ErrCode = iota
	ErrNotFound
	ErrAccessViolation
	ErrDiskFull
	ErrIllegalOp
	ErrUnknownID
	ErrFileExists
	ErrNoUser
)

const (
	NETASCII = "netascii"
	OCTET    = "OCTET"
	EMAIL    = "EMAIL"
)

type TFTPDataPacket struct {
	OpCode Opcode `default:"3"`
	Block  uint16
	Data   io.Reader
}

type TFTReadPacket struct {
	OpCode   Opcode `default:"1"`
	FileName string
	FirstPad byte   `default:"byte(0)"`
	Mode     string `default0:"NETASCII" default1:"OCTET" default2:"EMAIL"`
	EndPad   byte   `default:"byte(0)"`
}

type ReadRequest struct {
	FileName string
	Mode     string
}

type TFTWritePacket struct {
	opCode   Opcode `default:"2"`
	fileName string
	firstPad byte   `default:"byte(0)"`
	mode     string `default0:"netascii" default1:"octet" default2:"email"`
	endPad   byte   `default:"byte(0)"`
}

type TFTPAckPacket struct {
	opCode      Opcode `default:"3"`
	blockNumber uint16
}

type TFTPErrorPacket struct {
	opCode       Opcode `default:"3"`
	errorCode    uint16
	errorMessage string
	endPad       byte `default:"byte(0)"`
}

func (req *TFTReadPacket) MarshalBinary() ([]byte, error) { // used to setup client request
	mode := "octet"
	if req.Mode != "" {
		mode = req.Mode
	}

	// length of UDP data = operation code + fileName size + byte(0) + mode size + byte(0)
	cap := 2 + 2 + len(req.FileName) + 1 + len(mode) + 1
	b := new(bytes.Buffer)
	b.Grow(cap)

	err := binary.Write(b, binary.BigEndian, OpRRQ) // Write in the op code
	if err != nil {
		return nil, err
	}

	_, err = b.WriteString(req.FileName) // Now filename
	if err != nil {
		return nil, err
	}

	err = b.WriteByte(0) // write in delimiter
	if err != nil {
		return nil, err
	}

	_, err = b.WriteString(mode) // write in the mode to recieve data back in
	if err != nil {
		return nil, err
	}

	err = b.WriteByte(0) // write in delimiter
	if err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

func (req *TFTReadPacket) UnMarshalBinary(packet []byte) error { // used to read client request on server
	r := bytes.NewBuffer(packet)

	err := binary.Read(r, binary.BigEndian, req.OpCode)
	if err != nil {
		return errors.New("invalid Read Request!")
	}

	if req.OpCode != OpRRQ {
		return errors.New("Request is not a Read Request, wrong OpCode!")
	}

	req.FileName, err = r.ReadString(0) // Reminder: 0 here is a delimter, not a byte lenght
	if err != nil {
		return fmt.Errorf("The RRQ appears invalid error reading filename was %w", err)
	}

	req.FileName = strings.TrimRight(req.FileName, "\x00") // remove the 0-byte delimeter
	if len(req.FileName) == 0 {
		return fmt.Errorf("There was no filename sent, invalid RRQ!")
	}

	req.Mode, err = r.ReadString(0) // Reminder: 0 here is a delimter, not a byte lenght
	if err != nil {
		return fmt.Errorf("The RRQ appears invalid error reading mode was %w", err)
	}

	req.Mode = strings.TrimRight(req.Mode, "\x00") // remove the 0-byte delimeter
	if len(req.Mode) == 0 {
		return fmt.Errorf("There was no mode sent, invalid RRQ!")
	}

	return nil
}

func (data *TFTPDataPacket) MarshalBinary() ([]byte, error) {
	b := new(bytes.Buffer)
	b.Grow(DatagramSize)

	data.Block++ // block numbers start at 1, so assuming its 0 set, need to increment
	err := binary.Write(b, binary.BigEndian, OpData)
	if err != nil {
		return nil, err
	}

	err = binary.Write(b, binary.BigEndian, data.Block)
	if err != nil {
		return nil, err
	}

	// Now write in up to the allowed size of data
	_, err = io.CopyN(b, data.Data, BlockSize)
	if err != nil && err != io.EOF {
		return nil, err
	}

	return b.Bytes(), nil
}

func (data *TFTPDataPacket) UnMarshalBinary(packet []byte) error {
	if l := len(packet); l < 4 || l > DatagramSize {
		return errors.New("invalid DATA")
	}

	err := binary.Read(bytes.NewReader(packet[:2]), binary.BigEndian, data.OpCode)
	if err != nil || data.OpCode != OpData {
		return fmt.Errorf("The data packet was invalid, error if the opcode - %d wasn't wrong %w", data.OpCode, err)
	}

	err = binary.Read(bytes.NewReader(packet[2:4]), binary.BigEndian, &data.Block)
	if err != nil {
		return fmt.Errorf("Invalid data - %w", err)
	}

	data.Data = bytes.NewBuffer(packet[4:])

	return nil
}

func (ack *TFTPAckPacket) MarshalBinary() ([]byte, error) {
	b := new(bytes.Buffer)
	b.Grow(4) // the size of this is the opcode and the acknowledgement sequese

	err := binary.Write(b, binary.BigEndian, OpAck)
	if err != nil {
		return nil, fmt.Errorf("Erroring writing opcode - %w", err)
	}

	err = binary.Write(b, binary.BigEndian, ack.blockNumber)
	if err != nil {
		return nil, fmt.Errorf("Erroring writing block number - %w", err)
	}
	return nil, errors.New("Unimplemented method!")
}

func (ack *TFTPAckPacket) UnMarshalBinary(packet []byte) error {

	err := binary.Read(bytes.NewReader(packet[:2]), binary.BigEndian, ack.opCode)
	if err != nil || ack.opCode != OpAck {
		return fmt.Errorf("The data packet was invalid, error if the opcode - %d wasn't wrong %w", ack.opCode, err)
	}

	err = binary.Read(bytes.NewReader(packet[2:]), binary.BigEndian, ack.blockNumber)
	if err != nil {
		return fmt.Errorf("The data packet was invalid, error %w", err)
	}

	return nil
}

func (errPacket *TFTPErrorPacket) MarshalBinary() ([]byte, error) {
	size := 5 + len(errPacket.errorMessage)
	b := new(bytes.Buffer)
	b.Grow(size)

	err := binary.Write(b, binary.BigEndian, OpErr)
	if err != nil {
		return nil, fmt.Errorf("Erroring writing opcode - %w", err)
	}

	err = binary.Write(b, binary.BigEndian, errPacket.errorCode)
	if err != nil {
		return nil, fmt.Errorf("Error writing error code - %w", err)
	}

	_, err = b.WriteString(errPacket.errorMessage)
	if err != nil {
		return nil, fmt.Errorf("Error writing error message - %w", err)
	}

	err = b.WriteByte(0)
	if err != nil {
		return nil, fmt.Errorf("Error writing 0 pad - %w", err)
	}

	return b.Bytes(), nil
}

func (errPacket *TFTPErrorPacket) UnMarshalBinary(packet []byte) error {
	r := bytes.NewBuffer(packet)

	err := binary.Read(r, binary.BigEndian, &errPacket.opCode)
	if err != nil {
		return fmt.Errorf("The data packet was invalid, error reading the opcode - %d", err)
	}

	if errPacket.opCode != errPacket.opCode {
		return fmt.Errorf("Invalid packet for error, packet was %b", packet)
	}

	err = binary.Read(r, binary.BigEndian, &errPacket.errorCode)
	if err != nil {
		return fmt.Errorf("Error reading errorcode - %w", err)
	}

	errPacket.errorMessage, err = r.ReadString(0)
	errPacket.errorMessage = strings.TrimRight(errPacket.errorMessage, "\x00")
	return err
}
