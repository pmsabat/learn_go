package chapter6

import (
	"flag"
	"io/ioutil"
	"log"
)

var (
	addr    = flag.String("a", "127.0.0.1:69", "address and port to listen to [aaa.aaa.aaa.aaa:pppp]")
	payload = flag.String("p", "payload.svg", "The file to serve to clients")
)

func main() {
	flag.Parse()
	p, err := ioutil.ReadFile(*payload)
	if err != nil {
		log.Fatal(err)
	}

	s := Server{Payload: p}
	log.Fatal(s.ListenAndServe(*addr))
}
