package chapter4

import (
	"encoding/binary"
	"io"
)

type Binary []byte

func (m Binary) Bytes() []byte  { return m }
func (m Binary) String() string { return string(m) }

func (m Binary) WriteTo(w io.Writer) (int64, error) {
	err := binary.Write(w, binary.BigEndian, BinaryType) // 1-byte type
	if err != nil {
		return 0, err
	}

	var n int64 = 1
	err = binary.Write(w, binary.BigEndian, uint32(len(m)))
	if err != nil {
		return n, err
	}

	n += 4
	o, err := w.Write(m) //

	return n + int64(o), err
}

func (m *Binary) ReadFrom(r io.Reader) (int64, error) {
	var typ uint8
	err := binary.Read(r, binary.BigEndian, &typ)
	if err != nil {
		return 0, err
	}

	var n int64

	n, err = (*m).ReadWithKnownType(r)
	if err != nil {
		return n, err
	}
	return n, err
}

func (m *Binary) ReadWithKnownType(r io.Reader) (int64, error) {
	var n int64 = 5
	var size uint32
	err := binary.Read(r, binary.BigEndian, &size)
	if err != nil {
		return n, err
	}
	n += 4
	if size > MaxPayloadSize {
		return n, ErrMaxPayloadSize
	}

	n, err = (*m).ReadWithKnownSize(r, size)
	if err != nil {
		return n, err
	}
	// fmt.Printf("done reading fmt string %s\n", *m)
	return n, err
}

func (m *Binary) ReadWithKnownSize(r io.Reader, size uint32) (int64, error) {
	var n int64 = 5
	buf := make([]byte, size)
	o, err := r.Read(buf)
	if err != nil {
		return n, err
	}

	*m = buf

	return n + int64(o), err
}
