package chapter4

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

const (
	BinaryType uint8 = iota + 1
	StringType
	MaxPayloadSize uint32 = 10 << 20 // This is 10MB
)

var ErrMaxPayloadSize = errors.New("The max payload size was exceeded!")

type ReadWithKnownSize interface {
	ReadWithKnownSize(r io.Reader, size uint32) (int64, error)
}

type ReadWithKnownType interface {
	ReadWithKnownType(r io.Reader) (int64, error)
}

type Payload interface {
	fmt.Stringer
	io.ReaderFrom
	io.WriterTo
	ReadWithKnownSize
	ReadWithKnownType
	Bytes() []byte
}

func decode(r io.Reader) (Payload, error) {
	var typ uint8
	err := binary.Read(r, binary.BigEndian, &typ)
	if err != nil {
		return nil, err
	}

	var payload Payload
	switch typ {
	case BinaryType:
		payload = new(Binary)
	case StringType:
		payload = new(String)
	default:
		return nil, errors.New("Unknown type!")
	}

	_, err = payload.ReadWithKnownType(r)
	if err != nil {
		return nil, err
	}

	return payload, nil
}
