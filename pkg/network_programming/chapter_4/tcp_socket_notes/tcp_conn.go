package chapter4

import (
	"crypto/rand"
	"io"
	"log"
	"net"
	"time"
)

func OverRideTCPConnectionParams() {
	payload := make([]byte, 1<<24) // 2^24 - aka This is 16MB buffer
	_, err := rand.Read(payload)
	if err != nil {
		log.Fatal(err)
	}

	addr, err := net.ResolveTCPAddr("tcp", "127.0.0.1:") // get the socket address
	if err != nil {
		log.Fatal(err)
	}
	listener, err := net.ListenTCP("tcp", addr) // setup the listener to list for TCP
	if err != nil {
		log.Fatal(err)
	}
	tcpListenerConn, err := listener.AcceptTCP() // get the raw connection TCP socket
	if err != nil {
		log.Fatal(err)
	}

	tcpListenerConn.SetKeepAlive(true)              // set if you want a keep alive
	tcpListenerConn.SetKeepAlivePeriod(time.Second) // set at what frequency the keep alive should be

	go func() {
		conn, err := listener.Accept()
		rawTCPConn := conn.(*net.TCPConn)
		rawTCPConn.SetLinger(-1) // anything below 0 is default behavior, otherwise allows you to change linger behavior for last packet
		// rawTCPConn.SetLinger(0) // for example, this immediately discards the unsent data and closes the connection
		// rawTCPConn.SetLinger(10) // for example, this (after 10 seconds) discards the unsent data and closes the connection
		if err != nil {
			log.Println(err)
			return
		}

		defer conn.Close() // make sure to close out the connection once we're done
		// ^ If you fail to do this, you can cause the socket to go into a CLOSE_WAIT state

		_, err = conn.Write(payload) // write our payload of 16MB to the TCP connection
		if err != nil {
			log.Println(err)
		}
	}()

	conn, err := net.DialTCP("tcp", nil, addr) // connect to our listener

	if err != nil {
		log.Fatal(err)
	}

	buf := make([]byte, 1<<19) // 2^19 - aka 512 KB

	for {
		n, err := conn.Read(buf) // read off the connection
		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			break
		}

		log.Printf("I read %d bytes", n)
	}

	conn.Close()

}
