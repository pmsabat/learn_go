package chapter4

import (
	"io"
	"log"
	"net"
	"os"
)

type Monitor struct {
	*log.Logger
}

func (m *Monitor) Write(p []byte) (int, error) {
	err := m.Output(2, string(p))
	if err != nil {
		log.Printf("[Error] %s \n", err)
	}

	// TODO: Add some way to tell if an error should be critical, critical errors are the 2nd error
	return len(p), nil
}

func ExampleMonitor() {
	monitor := &Monitor{Logger: log.New(os.Stdout, "monitor", 0)}
	listener, err := net.Listen("tcp", "127.0.0.1:")
	if err != nil {
		monitor.Fatal(err)
	}

	done := make(chan struct{})

	go func() {
		defer close(done)
		conn, err := listener.Accept()
		if err != nil {
			monitor.Printf("[Error] %s \n", err)
			return
		}

		defer conn.Close()

		b := make([]byte, 1024) // 1KB
		r := io.TeeReader(conn, monitor)

		n, err := r.Read(b)
		if err != nil && err != io.EOF {
			monitor.Printf("[Error] %s \n", err)
			return
		}

		w := io.MultiWriter(conn, monitor)

		_, err = w.Write(b[:n]) // echo the message
		if err != nil && err != io.EOF {
			monitor.Printf("[Error] %s \n", err)
			return
		}
	}()

	conn, err := net.Dial(listener.Addr().Network(), listener.Addr().String())
	if err != nil {
		monitor.Fatal(err)
	}

	_, err = conn.Write([]byte("Test\n"))
	if err != nil {
		monitor.Fatal(err)
	}

	_ = conn.Close()
	<-done
}
