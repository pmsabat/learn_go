package chapter4

import (
	"crypto/rand"
	"io"
	"net"
	"testing"
)

func TestReadIntoBuffer(t *testing.T) {
	payload := make([]byte, 1<<24) // 2^24 - aka This is 16MB buffer
	_, err := rand.Read(payload)
	if err != nil {
		t.Fatal(err)
	}

	listener, err := net.Listen("tcp", "127.0.0.1:") // setup a listener for connections from localhost
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		conn, err := listener.Accept()
		if err != nil {
			t.Log(err)
			return
		}

		defer conn.Close() // make sure to close out the connection once we're done

		_, err = conn.Write(payload) // write our payload of 16MB to the TCP connection
		if err != nil {
			t.Error(err)
		}
	}()

	conn, err := net.Dial("tcp", listener.Addr().String()) // connect to our listener

	if err != nil {
		t.Fatal(err)
	}

	buf := make([]byte, 1<<19) // 2^19 - aka 512 KB

	for {
		n, err := conn.Read(buf) // read off the connection
		if err != nil {
			if err != io.EOF {
				t.Error(err)
			}
			break
		}

		t.Logf("I read %d bytes", n)
	}

	conn.Close()

}
