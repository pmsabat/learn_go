package chapter4

import (
	"io"
	"net"
)

func proxyConnection(source, destination string) error {
	connSource, err := net.Dial("tcp", source)
	if err != nil {
		return err
	}

	defer connSource.Close()

	connDest, err := net.Dial("tcp", destination)
	if err != nil {
		return err
	}
	defer connDest.Close()

	go func() { _, _ = io.Copy(connSource, connDest) }() //sends from dest to source

	_, err = io.Copy(connDest, connSource) //sends from source to dest

	return err
}
