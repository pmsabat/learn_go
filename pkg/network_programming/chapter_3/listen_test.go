package network_programming_chapter_3

import (
	"context"
	"fmt"
	"io"
	"net"
	"sync"
	"syscall"
	"testing"
	"time"
)

const unroutableAddress = "10.0.0.1:http"

func TestDial(t *testing.T) {
	listener, err := net.Listen("tcp", "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}

	defer func() { _ = listener.Close() }()

	t.Logf("Bound to %q", listener.Addr())
	done := make(chan struct{})
	go func() {
		defer func() { done <- struct{}{} }()
		for {
			conn, err := listener.Accept() // accept incoming connections on our listener
			if err != nil {
				t.Log(err) // something has gone wrong
				return
			}
			go func(c net.Conn) {
				defer func() {
					c.Close()
					done <- struct{}{}
				}()
				buf := make([]byte, 1024) //setup a buffer to read in the bytes from our channel
				for {
					n, err := c.Read(buf) // This is going to read in a kilobyte of data from the connection
					if err != nil {
						if err != io.EOF { // if our io reader says it has more, then something is wrong
							t.Error(err)
						}
						return
					}
					t.Logf("received %q", buf[:n])
				}
			}(conn)
		}
	}()

	conn, err := net.Dial("tcp", listener.Addr().String())
	if err != nil {
		t.Fatal(err)
	}
	conn.Close()
}

func TestDialTimeout(t *testing.T) {
	c, err := DialWithTimeout("tcp", "10.0.0.1:http", 5*time.Second)
	if err == nil {
		c.Close()
		t.Fatal("connection did not time out!")
	}
	nErr, ok := err.(net.Error)
	if !ok {
		t.Fatal(err)
	}
	if !nErr.Timeout() {
		t.Fatal(fmt.Sprintf("error is not a timeout, was %s", err))
	}
}

func TestDialTimeoutContext(t *testing.T) {
	delay_in_seconds := time.Duration(1)
	dl := time.Now().Add(delay_in_seconds * time.Second)
	ctx, cancel := context.WithDeadline(context.Background(), dl) //give it a context in the background with enough time
	defer cancel()                                                // allow this to be garbage collected

	var dialer net.Dialer                                         // setup our dialer struct to enable the test
	dialer.Control = func(_, _ string, _ syscall.RawConn) error { // force the dialer to sleep every time its called
		time.Sleep(delay_in_seconds*time.Second + (time.Millisecond * time.Duration(10))) // here is the amount of sleep!
		return nil
	}

	conn, err := dialer.DialContext(ctx, "tcp", unroutableAddress) // call TCP on normal http port for 10.0.0.1 - will fail
	if err == nil {
		conn.Close()
		t.Fatal("connection did not time out like it should!")
	}

	nErr, ok := err.(net.Error) // we should have received a net error!
	if !ok {
		t.Error(err)
	} else {
		if !nErr.Timeout() {
			t.Errorf("The error is not a timeout: %v", err)
		}
	}

	if ctx.Err() != context.DeadlineExceeded {
		t.Errorf("Expected deadline to exceed, instead %v", ctx.Err()) // If we didn't except the context deadline but got a timeout error, something is weird
	}
}

func TestDialContextCancelling(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background()) //give it a context in the background with enough time
	sync := make(chan struct{})

	go func() { // This goroutine handles the dialer/making sure it times out
		defer func() { sync <- struct{}{} }()

		var dialer net.Dialer
		dialer.Control = func(_, _ string, _ syscall.RawConn) error {
			time.Sleep(time.Second)
			return nil
		}

		conn, err := dialer.DialContext(ctx, "tcp", unroutableAddress)
		if err != nil {
			t.Log(err)
			return
		}

		conn.Close()
		t.Error("The connection didn't time out, we should not have gotten here!")
	}()

	cancel() // ok lets kill this so the context cancels
	<-sync
	if ctx.Err() != context.Canceled {
		t.Errorf("The context should have been canceled, was not: %v", ctx.Err())
	}
}

func TestDialContextCancellingWithFanout(t *testing.T) {
	delay_in_seconds := time.Duration(3)
	dl := time.Now().Add(delay_in_seconds * time.Second)
	ctx, cancel := context.WithDeadline(context.Background(), dl) //give it a context in the background with enough time

	listener, err := net.Listen("tcp", "127.0.0.1:")
	if err != nil {
		t.Fatalf("Setting up the listener failed, error was: %v", err)
	}

	defer listener.Close()

	go func() {
		conn, err := listener.Accept()
		if err == nil { // Got a connection, close it
			conn.Close()
		}
	}()

	dial := func(ctx context.Context, address string, response chan int, id int, wg *sync.WaitGroup) {
		defer wg.Done()

		var dialer net.Dialer

		c, err := dialer.DialContext(ctx, "tcp", address)
		if err != nil {
			return
		}
		c.Close()
		select {
		case <-ctx.Done():
		case response <- id:
		}
	}

	res := make(chan int)
	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go dial(ctx, listener.Addr().String(), res, i+1, &wg)
	}
	response := <-res
	cancel()
	wg.Wait()
	close(res)

	if ctx.Err() != context.Canceled {
		t.Errorf("The context should have been canceled, was not: %v", ctx.Err())
	}

	t.Logf("dialer %d retrieved the resource", response)
}
