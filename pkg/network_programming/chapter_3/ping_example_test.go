package network_programming_chapter_3

import (
	"context"
	"fmt"
	"io"
	"net"
	"testing"
	"time"
)

func ExamplePinger() {
	ctx, cancel := context.WithCancel(context.Background())
	r, w := io.Pipe()
	done := make(chan struct{})
	resetTimer := make(chan time.Duration, 1)
	resetTimer <- time.Second

	go func() {
		Pinger(ctx, w, resetTimer)
		close(done)
	}()

	receivePing := func(d time.Duration, r io.Reader) {
		if d >= 0 {
			fmt.Printf("resetting timer (%s)\n", d)
			resetTimer <- d
		}

		now := time.Now()
		buf := make([]byte, 1024)
		n, err := r.Read(buf)
		if err != nil {
			fmt.Printf("Error %v \n", err)
		}
		fmt.Printf("received %q (%s)\n", buf[:n], time.Since(now).Round(100*time.Millisecond))
	}

	for i, v := range []int64{0, 200, 300, 0, -1, -1, -1} {
		fmt.Printf("Run %d \n", i+1)
		receivePing(time.Duration(v)*time.Millisecond, r)
	}

	cancel()
	<-done
}

func TestPingerAdvanceDeadline(t *testing.T) {
	done := make(chan struct{})
	listener, err := net.Listen("tcp", "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}
	begin := time.Now()
	go func() {
		defer func() { close(done) }() //closes the channel as the last thing
		conn, err := listener.Accept()
		if err != nil {
			t.Log(err)
			return
		}
		ctx, cancel := context.WithCancel(context.Background())
		defer func() { // This function is going to close out the conext and any attached processes/functions when we're done
			cancel()
			conn.Close()
		}()

		resetTimer := make(chan time.Duration, 1)
		resetTimer <- time.Second
		go Pinger(ctx, conn, resetTimer)

		err = conn.SetDeadline(time.Now().Add(5 * time.Second))
		if err != nil {
			t.Error(err)
			return
		}

		buf := make([]byte, 1024)
		for {
			n, err := conn.Read(buf)
			if err != nil {
				return
			}
			t.Logf("[%s] Pinger Got Back: %s", time.Since(begin).Truncate(time.Second), buf[:n])

			resetTimer <- 0                                         // tells pinger to reset the timer
			err = conn.SetDeadline(time.Now().Add(5 * time.Second)) // push our connection deadline by 5 seconds

			if err != nil {
				t.Error(err)
				return
			}
		}
	}()

	conn, err := net.Dial("tcp", listener.Addr().String())

	if err != nil {
		t.Fatal(err)
	}

	defer conn.Close()

	sender_buf := make([]byte, 1024)
	for i := 0; i < 4; i++ {
		n, err := conn.Read(sender_buf)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("[%s], Dialer got: %s", time.Since(begin).Truncate(time.Second), sender_buf[:n])
	}

	_, err = conn.Write([]byte("PONG!!!"))
	if err != nil {
		t.Fatal(err)
	}

	for i := 0; i < 4; i++ {
		n, err := conn.Read(sender_buf)
		if err != nil {
			if err != io.EOF {
				t.Fatal(err)
			}
			break
		}
		t.Logf("[%s], Dialer got after PONG: %s", time.Since(begin).Truncate(time.Second), sender_buf[:n])
	}
	<-done
	end := time.Since(begin).Truncate(time.Second)
	t.Logf("[%s] done", end)
	if end != 9*time.Second {
		t.Fatalf("expected to end test at 9 seconds, actual time: %s", end)
	}
}
