package network_programming_chapter_3

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

const defaultPingInterval = time.Duration(3) * time.Second

var (
	count    = flag.Int("c", 3, "number of pings: <= 0 is forever")
	interval = flag.Duration("i", defaultPingInterval, "interval between pings default is 3 seconds")
	timeout  = flag.Duration("W", defaultPingInterval, "interval between pings default is 3 seconds")
)

func init() {
	flag.Usage = func() {
		fmt.Printf("Usage: %s [options] host:port \nOptions: \n", os.Args[0])
		flag.PrintDefaults()
	}
}

func Pinger(ctx context.Context, w io.Writer, reset <-chan time.Duration) {
	var interval time.Duration
	select {
	case <-ctx.Done():
		return
	case interval = <-reset:
	default:
	}
	if interval <= 0 {
		interval = defaultPingInterval
	}

	timer := time.NewTimer(interval)

	defer func() {
		if !timer.Stop() {
			<-timer.C
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case newInterval := <-reset:
			if !timer.Stop() {
				<-timer.C
			}
			if newInterval > 0 {
				interval = newInterval
			}
		case <-timer.C:
			if _, err := w.Write([]byte("PING")); err != nil {
				return
			}
		}
		_ = timer.Reset(interval)
	}

}

func main() {
	flag.Parse()

	if flag.NArg() != 1 {
		fmt.Print("host:post is required!")
		flag.Usage()
		os.Exit(0)
	}

	target := flag.Arg(0)
	fmt.Println("Ping", target)

	if *count <= 0 {
		fmt.Println("Ctrl+C to stop!")
	}

	msg := 0
	for (*count <= 0) || (msg < *count) {
		msg++
		fmt.Print("msg", " ")
		start := time.Now()
		c, err := net.DialTimeout("tcp", target, *timeout)
		dur := time.Since(start)

		if err != nil {
			fmt.Printf("Fail in %s: %s \n", dur, err)
			if nErr, ok := err.(net.Error); !ok || !nErr.Temporary() {
				os.Exit(1)
			}
		} else {
			_ = c.Close()
			fmt.Printf("The duration was: %d \n", dur)
		}
		time.Sleep(*interval)
	}
}
