package chapter7

import (
	"context"
	"fmt"
	"net"
)

func streamingEchoServer(ctx context.Context, network, addr string) (net.Addr, error) {
	s, err := net.Listen(network, addr)
	if err != nil {
		return nil, fmt.Errorf("binding to udp %s -- failed: %w", addr, err)
	}

	go func() {
		go func() {
			<-ctx.Done()
			_ = s.Close()
		}()

		for {
			conn, err := s.Accept()
			if err != nil {
				return
			}
			for {
				buf := make([]byte, 1024)
				n, err := conn.Read(buf)
				if err != nil {
					return
				}

				_, err = conn.Write(buf[:n])
				if err != nil {
					return
				}

			}

		}
	}()

	return s.Addr(), nil

}
