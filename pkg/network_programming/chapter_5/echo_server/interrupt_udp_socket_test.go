package chapter5

import (
	"bytes"
	"context"
	"net"
	"testing"
)

func TestListenPacketUDP(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	serverAddr, err := echoServerUDP(ctx, "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}

	defer cancel()
	client, err := net.ListenPacket("udp", "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}
	defer func() { _ = client.Close() }()

	interloper, err := net.ListenPacket("udp", "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}

	interrupt_msg := []byte("pardon me!")
	n, err := interloper.WriteTo(interrupt_msg, client.LocalAddr())
	if err != nil {
		t.Fatal(err)
	}

	_ = interloper.Close()
	if l := len(interrupt_msg); l != n {
		t.Fatalf("Wrote %d bytes of %d", l, n)
	}

	msg := []byte("ping")
	_, err = client.WriteTo(msg, serverAddr)
	if err != nil {
		t.Fatal(err)
	}

	buf := make([]byte, 1024)
	n, addr, err := client.ReadFrom(buf)
	if err != nil {
		t.Fatal(err)
	}

	if addr.String() != interloper.LocalAddr().String() { // note that we sent this to the CLIENT, so client will recieve the interrupt before it sees the reply from the server since its socket was listening
		t.Errorf("Expected reply from %q and instead got it from %q - stop other network processes?", interloper.LocalAddr().String(), addr)
	}
	if !bytes.Equal(interrupt_msg, buf[:n]) {
		t.Errorf("Expected reply to be %q, was actually %q", interrupt_msg, buf[:n])
	}

	// move to next client
	n, addr, err = client.ReadFrom(buf)
	if err != nil {
		t.Fatal(err)
	}

	if addr.String() != serverAddr.String() { // now we get to the actual message
		t.Errorf("Expected reply from %q and instead got it from %q - stop other network processes?", client.LocalAddr().String(), addr)
	}
	if !bytes.Equal(msg, buf[:n]) {
		t.Errorf("Expected reply to be %q, was actually %q", msg, buf[:n])
	}
}
