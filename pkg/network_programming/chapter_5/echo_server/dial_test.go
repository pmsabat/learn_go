package chapter5

import (
	"bytes"
	"context"
	"net"
	"testing"
	"time"
)

func TestDialUP(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	serverAddr, err := echoServerUDP(ctx, "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}
	defer cancel()

	client, err := net.Dial("udp", serverAddr.String())
	if err != nil {
		t.Fatal(err)
	}
	defer func() { _ = client.Close() }()

	interloper, err := net.ListenPacket("udp", "127.0.0.1:")
	if err != nil {
		t.Fatal(err)
	}

	interrupt_msg := []byte("pardon me!")
	n, err := interloper.WriteTo(interrupt_msg, client.LocalAddr())
	if err != nil {
		t.Fatal(err)
	}

	_ = interloper.Close()
	if l := len(interrupt_msg); l != n {
		t.Fatalf("Wrote %d bytes of %d", l, n)
	}

	ping := []byte("ping")
	_, err = client.Write(ping)
	if err != nil {
		t.Fatal(err)
	}

	buf := make([]byte, 1024)
	read_bytes, err := client.Read(buf)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(ping, buf[:read_bytes]) {
		t.Errorf("Expected reply to be %q, was actually %q", interrupt_msg, buf[:read_bytes])
	}

	err = client.SetDeadline(time.Now().Add(time.Second))
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Read(buf)
	if err == nil {
		t.Fatal("Unexpected packet, should not have recieved!")
	}
}
