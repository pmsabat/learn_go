package main

import (
	"fmt"

	"gitlab.com/pmsabat/learn_go/pkg/exercises"
	"gitlab.com/pmsabat/learn_go/pkg/models"
	network_programming_chapter_3 "gitlab.com/pmsabat/learn_go/pkg/network_programming/chapter_3"
	"golang.org/x/tour/tree"
)

type Player = models.Player
type User = models.User

func main() {
	var i int
	i = 42

	var f float32 = 3.14
	firstName := "King"
	lastName := "Arthur"
	home := "Camelot"
	ptr := &i
	fmt.Println("hello new guys", (float32(i) * f), "his name is", firstName, lastName, "of", home, "; at ", ptr, "with value", *ptr) // TYPES

	p := Player{
		User:   &User{Id: 42, Name: "Matt", Location: "LA"},
		GameId: 90404,
	}
	fmt.Printf(
		"Id: %d, Name: %s, Location: %s, Game id: %d\n",
		p.Id, p.Name, p.Location, p.GameId)

	fmt.Println(p.Greetings())
	// Directly set a field defined on the Player struct
	// p.Id = 11
	// fmt.Printf("%+v", p)

	// testListener(&testing.T{})
	bitcoin_names := []string{"Matthew", "Sarah", "Augustus", "Heidi", "Emilie", "Peter", "Giana", "Adriano", "Aaron", "Elizabeth"}
	var names = []string{"Katrina", "Evan", "Neil", "Adam", "Martin", "Matt",
		"Emma", "Isabella", "Emily", "Madison",
		"Ava", "Olivia", "Sophia", "Abigail",
		"Elizabeth", "Chloe", "Samantha",
		"Addison", "Natalie", "Mia", "Alexis"}

	sorted_names := exercises.QuickSort(names)
	fmt.Println("The names were", sorted_names)
	fmt.Println("The bitcoin will be distributed as follows: ", exercises.DistributeBitcoin(bitcoin_names, 50))

	exercises.WCTest()
	fmt.Println(fmt.Sprintf("Tree(1)=Tree(1) was %t", exercises.Same(tree.New(1), tree.New(1))))
	fmt.Println(fmt.Sprintf("Tree(1)=Tree(2) was %t", exercises.Same(tree.New(1), tree.New(2))))

	fmt.Printf("The unroutable address is: %s", network_programming_chapter_3.UnroutableAddress())
}
